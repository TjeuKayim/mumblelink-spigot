package com.github.tjeukayim.mumblelinkspigot;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class MumblePlugin extends JavaPlugin implements Listener {
    public static final String CHANNEL = "fabric-mumblelink-mod:broadcast_mumble_url";
    private final FileConfiguration config = getConfig();

    public static final String MUMBLE_SERVER_HOST = "mumbleServerHost";
    public static final String MUMBLE_SERVER_PORT = "mumbleServerPort";
    public static final String LAUNCH_ON_CONNECT = "launchOnConnect";

    @Override
    public void onEnable() {
        config.addDefault(MUMBLE_SERVER_HOST, "");
        config.addDefault(MUMBLE_SERVER_PORT, -1);
        config.addDefault(LAUNCH_ON_CONNECT, true);
        // TODO: config.addDefault("registerCommand", false);

        config.options().copyDefaults(true);
        saveConfig();

        getServer().getPluginManager().registerEvents(this, this);
        getServer().getMessenger().registerOutgoingPluginChannel(this, CHANNEL);
    }

    @EventHandler
    public void afterPlayerJoin(PlayerJoinEvent event) {
        if (!config.getBoolean(LAUNCH_ON_CONNECT)) return;
        Player player = event.getPlayer();
        getServer().getScheduler().runTaskLaterAsynchronously(this, () -> {
            String host = config.getString(MUMBLE_SERVER_HOST);
            int port = config.getInt(MUMBLE_SERVER_PORT);
            getLogger().info("Mumble auto launch for player " + player.getName() + " host " + host + ":" + port);
            byte[] data = craftPacket(host, port);
            player.sendPluginMessage(this, CHANNEL, data);
        }, 128);
    }

    public static byte[] craftPacket(String host, int port) {
        // based on https://github.com/magneticflux-/fabric-mumblelink-mod/blob/b6ffa961f070ae17241ecb9f5d2a1a7fb91c9cdd/src/main/kotlin/com/skaggsm/mumblelinkmod/MumbleLinkMod.kt#L65

        VoipClient voipClient = VoipClient.MUMBLE;
        String path = "";
        String query = "";

        ByteBuf buf = Unpooled.buffer();
        ProtocolUtils.writeEnumConstant(buf, voipClient);
        ProtocolUtils.writeString(buf, host);
        buf.writeInt(port);
        ProtocolUtils.writeString(buf, path);
        ProtocolUtils.writeString(buf, query);

        byte[] data = new byte[buf.writerIndex()];
        buf.getBytes(0, data);
        buf.release();
        return data;
    }

    @SuppressWarnings("unused")
    enum VoipClient {
        MUMBLE,
        TEAMSPEAK
    }
}
